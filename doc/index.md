functmpl 使用说明  
========  
1. 语法  
   1. [functmpl标签](syntax/functmpl.md)  
   2. [脚本](syntax/script.md)  
2. API  
   1. [functmpl](api/functmpl.md)  
   2. [作用域](api/scope.md)  
   3. [脚本](api/script.md)  
3. [内置<@功能/>](func.md)  
