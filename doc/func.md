内置功能处理器  
========
内置功能处理器以功能名称为键，保存在functmpl.func中  

##enum  
```
<@enum=list key="key" value="value"></>
```

枚举列表：创建局部作用区域，对list中的内容进行枚举，将索引保存在变量key中，将值保存在变量value中  
参数说明  
* `@enum`{any} - 被枚举的对象  
* `value`{String} - 值所要保存到的变量的变量名  
* `key`{String} - *可选的*索引所要保存到的变量的变量名  

##scope  
```
<@scope></>
```
创建局部作用域  

##set  
```
<@set="key" value="1" type="create"/>
```
创建变量/变量赋值  
参数说明  
* `@set`{String} - 变量名  
* `value`{any} - 值  
* `type`{String} - *可选的*方法：create创建变量/delete 删除变量/set变量赋值,默认为set  

##if  
```
<@if=``条件``>
<|elseif=``条件``>
<|else>
</>
```
分歧语句, 类似与C语言的:  

```
if (条件) {
} else if (条件) {
} else {
}
```
参数说明  
* `@if`{Script} - 条件语句  
* `|elseif`{Script} - 条件语句  

##while  
```
<@while=``条件``></>
```
循环语句, 类似与C语言的`while(条件){}`  
参数说明:  
* `@while`{Script} - 条件语句  

##break  
```
<@break/>
```
结束循环, 类似与C语言的`break;`  

##continue  
```
<@continue/>
```
略过此次循环剩余的语句, 类似与C语言的`continue;`  

