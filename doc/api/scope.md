作用域
========
##创建作用域  
```
//无父作用域
let scope = functmpl.scope();
//有父作用域
let scope = functmpl.scope(parentScope);
```

##设置变量  
`scope[key] = value`  
参数说明:  
* `key`{String} - 变量名  
* `value`{any} - 变量值  

##获取变量值  
`scope[key]`  
`参数说明:  
* `key`{String} - 变量名  
`
##创建变量  
`scope[functmpl.scope.create](key,value)`  
参数说明:  
* `key`{String} - 变量名  
* `value`{any} - 变量值  

##删除变量  
`delete scope[key]`  
参数说明:  
* `key`{String} - 变量名  

