脚本  
========  
##functmpl.script(script)  
创建脚本  
参数说明:  
* `script`{String} - 脚本代码  

##script.value  
`只读属性`获取脚本执行的结果  

##async script.exec(scope)  
**异步**执行脚本  
参数说明:  
* `scope`{Scope} - 作用域  

##functmpl.script.is(script)  
判读是否为脚本  


