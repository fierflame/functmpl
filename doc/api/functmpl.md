##functmpl([prtFtl])  
创建functmpl对象ftl  
参数说明  
* `prtFtl`{Functmpl} - *可选的*父模板  

##ftl.filename = name  
设置文件名  
* `name`{String} - 文件名  

如果设置失败，将抛出错误  
##async ftl.setTemplate (text)  
**异步**设置模板  
参数说明  
* `text`{String} - 模板文本  

如果设置失败，将抛出错误  

##async ftl.loadFile (filename)  
**异步**加载模板文件  
参数说明
* `filename`{String} - 模板文件名  

如果加载设置失败，将抛出错误  

##ftl.use (func, [type]) {  
*同步*添加处理函数  
参数说明  
* `中间件`{Function} - 中间件  
* `type`{String | Symbol} - *可选的*处理内容  


##async ftl.parse ([...param])  
**异步**解析  
参数说明  
* `param`{...Object} - *可选的*传入的作用域或数值  

返回值为解析后文本  

##async ftl.parseList (list, scope)  
**异步**解析列表  
参数说明  
* `list`{Array} - 列表  
* `scope`{Scope} - 作用域  

##async ftl.parseNode(node, scope)  
**异步**解析节点  
参数说明  
* `node`{Object} 节点信息  
* `scope`{Scope} - 作用域  

##async ftl.repeat (list, scope, data, [options])  
`工具`**异步**将列表重复执行  
参数说明  
* `list`{Array} - 列表  
* `scope`{Scope} - 作用域  
* `data`{Object} - 数据  
* `options`{Object} - *可选的*选项  

有效的选项如下：  
|选项		|类型		|默认值	|说明				|  
|---------------------------------------------------|  
|key		|String		|""		|键保存的变量名		|  
|value		|String		|""		|至保存的变量名		|  
|num		|String		|""		|总数保存的变量名		|  
|release	|Boolean	|false	|是否将值展开			|  
|noBreak	|Boolean	|false	|是否不处理break		|  
|noContinue	|Boolean	|false	|是否不处理continue	|  
|base		|Object		|null	|将展开的基本数据		|  
