'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _process = require('./func/process');

var _process2 = _interopRequireDefault(_process);

var _variable = require('./func/variable');

var _variable2 = _interopRequireDefault(_variable);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const func = { 'process': _process2.default, 'variable': _variable2.default };
exports.default = func;


for (let k in _process2.default) {
	func[k] = _process2.default[k];
}
for (let k in _variable2.default) {
	func[k] = _variable2.default[k];
}