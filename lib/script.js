'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _scope = require('./scope');

var _scope2 = _interopRequireDefault(_scope);

var _vm = require('vm');

var _vm2 = _interopRequireDefault(_vm);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class Script {
	constructor(script) {
		this._script = new _vm2.default.Script(script);
	}
	async exec(s) {
		return this._value = await this._script.runInContext(_vm2.default.createContext((0, _scope2.default)(s)));
	}
	get value() {
		return this._value;
	}
}
function script(script) {
	return new Script(script);
}
script.is = e => e instanceof Script;
exports.default = script;