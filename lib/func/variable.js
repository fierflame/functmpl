'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});
const func = {};
exports.default = func;


func['scope'] = { type: 'scope', async func(node, scope) {
		await this.parseList(node.list, this.scope(scope));
	} };

func['set'] = { type: 'set', async func(node, scope) {
		let key = node.value,
		    value = node.param.value,
		    type = node.param.type;
		if (type === 'create') {
			scope[this.scope.create](key, value);
		} else if (type === 'delete') {
			delete scope[key];
		} else {
			scope[key] = value;
		}
	} };