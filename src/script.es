import scope from './scope';
import vm from 'vm';
class Script {
	constructor (script) {
		this._script = new vm.Script(script);
	}
	async exec (s) {
		return this._value = await this._script.runInContext(vm.createContext(scope(s)))
	}
	get value () {
		return this._value;
	}
}
function script (script) {
	return new Script(script);
}
script.is = e => e instanceof Script;
export default script;
