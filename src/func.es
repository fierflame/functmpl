import proc from './func/process';
import vari from './func/variable';

const func = {'process': proc, 'variable': vari};
export default func;

for(let k in proc) {
	func[k] = proc[k];
}
for(let k in vari) {
	func[k] = vari[k];
}

