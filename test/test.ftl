<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8"/>
	<title><`=title`></title>
</head>
<body>
	<table>
		<tr><td>用户</td><td>年龄</td></tr>
		<@enum=user key="i" value="user">
			<@scope>
				<@set="age" type="create" value=`user.age`/>
				<@if=``i == 1`>
					<@set="age" value=0 />
				</>
				<tr><td><`=user.name`></td><td><`=age`></td></tr>
			</>
		</>
	</table>
</body>
</html>