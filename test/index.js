const functmpl = require("../").default;
const fs = require("fs");

let f = functmpl();
let func = functmpl.func;
for (let k in func) {
	f.use(func[k]);
}
async function test() {
	await f.loadFile(__dirname + "/test.ftl");
	let data = await new Promise((a,b) => fs.readFile(__dirname + "/test.json", 'utf-8', (e,d)=>e?b(e):a(d)))
	data = JSON.parse(data);
	let html = await f.parse(data);
	console.log(html);
}
test().catch(console.log);
