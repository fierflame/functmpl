const scope = require("../lib/scope").default;
const vm = require("vm");


let create = scope.create;
let a1 = scope();
let a2 = scope(a1);
let a3 = scope(a2);
a1[create]("a1",6);
a2[create]("a2",6);
a3[create]("a3",6);

let c1 = vm.createContext(a1);
let c2 = vm.createContext(a2);
let c3 = vm.createContext(a3);
c2.b1 = 5;
c2.a1 = 7;

console.log(a1);
console.log(a2);
console.log(a3);
console.log(c1);
console.log(c2);
console.log(c3);
console.log(c3.a1);
