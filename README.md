functmpl
===============
```
   ___               _             _        _   
 _|  _|_ _ ___ ___ _| |_ _____ ___| |   _ _| |  
|_   _| | |   |  _|_   _|     | . | |  | | | |  
  |_| |___|_|_|___| |___|_|_|_|  _|_|   \_/|_|  
                              |_|               
```
[![npm|Xauto](https://img.shields.io/npm/v/functmpl.svg)](https://www.npmjs.com/package/functmpl)
[![npm|Xauto](https://img.shields.io/npm/l/functmpl.svg)](https://www.npmjs.com/package/functmpl)
[![npm|Xauto](https://img.shields.io/npm/dt/functmpl.svg)](https://www.npmjs.com/package/functmpl)
作者：wangchenxunum <wangchenxunum@qq.com>;
##关于functmpl
functmpl 是一个基于JavaScript/Nodejs的高扩展性的模板引擎  

##安装functmpl
使用npm 或者 git

```
npm install functmpl
git clone git@github.com:wangchenxunum/functmpl.git
git clone git@git.oschina.net:wangchenxunum/functmpl.git
```

##系统依赖
###生产环境

node 6.9.0 或更高版本。

###开发及测试环境

```
babel-cli: ^6.22.2  
babel-eslint: ^7.1.1  
babel-plugin-transform-async-to-generator: ^6.22.0  
babel-plugin-transform-es2015-modules-commonjs: ^6.22.0  
```
